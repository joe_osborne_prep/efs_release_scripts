#!/bin/bash
source EFS_Functions.sh

if [[ $# -ne 2 ]]
then
        echo please ensure you provide 2 arguments
        echo 1: The argument for META
        echo 2: The argument for PROJECT
else

	META=$1
	PROJECT=$2

	RELEASE=$(createName $META $PROJECT)
	makeRelease=$(createInitialRelease $META $PROJECT $RELEASE)

	efs create install $META $PROJECT $RELEASE common


	cp -R /efs/dev/$META/$PROJECT/$RELEASE/src/.   /efs/dev/$META/$PROJECT/$RELEASE/install/common 

	efs create releaselink $META $PROJECT $RELEASE ${LOGNAME}_DEV

fi

