#!/bin/bash

if [[ $# -ne 2 ]]
then
        echo please ensure you provide 2 arguments
        echo 1: The argument for META
	echo 2: The argument for PROJECT

else

	META=$1
	PROJECT=$2
	
	RELEASE=`readlink /efs/dev/teach/exercise/${LOGNAME}_DEV`


	efs checkpoint $META $PROJECT $RELEASE	

	efs dist releaselink $META $PROJECT ${LOGNAME}_DEV

	efs create releaselink $META $PROJECT $RELEASE ${LOGNAME}_UAT

	efs dist releaselink $META $PROJECT ${LOGNAME}_UAT

fi

